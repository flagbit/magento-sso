# Flagbit Magento SSO

Single Sign On implementation between Typo3 and Magento. Avoid the use third party system components like *Security Assertion Markup Language* (SAML) or *OpenId* to remain 
as self contained as possible. Whenever requirements gets elaborated start thinking on more specific solutions like mentioned above or LDAP integration for example.

# What is does

Communicate with Typo3 by sending signed requests containing user data. On Typo3 end the [typo3-fbsso](https://bitbucket.org/flagbit/typo3-fbsso) must be installed and properly 
configured with magento side public keys so signed information can be verified and user and/or session is created on Typo3.

### Important note:
The information sent to Typo3 is *only signed*, *not encripted*. Dont send sensible information unless the channel is secure (https or secured network).

# Install

Install by using composer as a magento module. Require `magento-hackathon/magento-composer-installer` and `flagbit/magento-sso` in your composer. The dependency 
to the [ssl wrapper library](https://bitbucket.org/flagbit/library-sso) must be declared also. Your composer should look like:

```
"require": {
        "magento-hackathon/magento-composer-installer": "^3.0",
        "flagbit/magento-sso": "*",
        "flagbit/library-sso": "*"
    },
    "extra": {
        "installer-paths": {
            "src/lib/": ["type:magento-library"]
        }
    },
```

